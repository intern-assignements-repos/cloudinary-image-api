import express from "express";
import dotenv from "dotenv";
import router from "./src/routes/image";

dotenv.config();

const app = express();

const port = process.env.PORT || 6000;
const host = process.env.HOST_NAME || "localhost";

app.use("/image", router);

app.listen(port, () => {
  console.log(`Server started at http://${host}:${port}`);
});
