import { Request, Response } from "express";
import errorHandler from "../utils/error";

const uploadImageController = (req: Request, res: Response) => {
  try {
    if (req.file) {
      return res.status(200).send({
        message: "Image uploaded successfully",
        imageUrl: req.file.path,
      });
    } else {
      return errorHandler(
        res,
        {
          message: "Please upload a valid Image file.",
        },
        400
      );
    }
  } catch (err: Error | any) {
    errorHandler(res, err, 500);
  }
};

export default uploadImageController;
