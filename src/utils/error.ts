import { Response } from "express";

const errorHandler = async (res: Response, err: Error | any, status: number) => {
  return res.status(status).send({
    message: err?.message || "Some error occurred while creating the User.",
  });
};

export default errorHandler;
