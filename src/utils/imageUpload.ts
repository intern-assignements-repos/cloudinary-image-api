import dotenv from "dotenv";
import multer from "multer";
import { v2 as cloudinary } from "cloudinary";
import cloudinaryStorage from "multer-storage-cloudinary";

dotenv.config();

cloudinary.config({
  api_key: process.env.API_KEY,
  api_secret: process.env.API_SECRET,
  cloud_name: process.env.CLOUD_NAME,
});

const MIME_TYPES = {
  "image/png": "png",
  "image/jpg": "jpg",
  "image/jpeg": "jpeg",
};

const storage = cloudinaryStorage({
  cloudinary: cloudinary,
  params: async (req, file) => {
    const extension =
      MIME_TYPES[file.mimetype as "image/png" | "image/jpeg" | "image/jpg"];

    if (!extension) throw new Error("File type not Supported!");

    return {
      folder: "node-auth-system",
      format: extension,
      public_id: `${new Date().toISOString()}`,
    };
  },
});

export default multer({ storage: storage });
