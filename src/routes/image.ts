import express from "express";

import upload from "../utils/imageUpload";
import uploadImageController from "../controller/imageController";

const router = express.Router();
router.post("/", upload.single("image"), uploadImageController);

export default router;
